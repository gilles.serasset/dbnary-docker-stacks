# maven-jena image

This image is a maven 3 + openjdk-11 image with JENA commands installed.

It has been created as an image for DBnary CICD settings as we compile the project, then evaluate it using 
some jena tools.

Create images : 
make build-multi/maven-jena

Distribute images : 
maven_jena_EXTRA_TAG_ARGS="-t serasset/maven-jena:jena-4.10.0 -t serasset/maven-jena:jena-4.10" make push-multi/maven-jena 
