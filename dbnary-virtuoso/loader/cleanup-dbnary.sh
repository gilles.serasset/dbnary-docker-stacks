#!/bin/bash

####################################################################################
# Clean up the dbnary data, i.e. extracted turtle files and corresponding graph files
# The original data is present in the /dbnary folder or downloaded from the internet
####################################################################################
#  Copyright (C) 2022-2022 Gilles Sérasset — Université Grenoble Alpes
cd ${DATASETS} && rm -f *.ttl *.bz2 *.graph