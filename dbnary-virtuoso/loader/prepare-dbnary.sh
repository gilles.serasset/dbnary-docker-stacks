#!/bin/bash

####################################################################################
# Prepare the dbnary data, i.e. extracted turtle files and corresponding graph files
# The original data is present in the /dbnary folder or downloaded from the internet
####################################################################################
#  Copyright (C) 2022-2022 Gilles Sérasset — Université Grenoble Alpes
#set -x

echo "================================================"
echo "PREPARING DBNARY DATA"
echo "================================================"

DEFAULT_FEATURES="ontolex morphology etymology enhancement lime statistics exolex_ontolex exolex_lime"
DEFAULT_LANGUAGES="fr en de pt it fi ru el tr ja es bg pl nl sh sv lt no mg id la ku"

## Standardize language codes to 3 letter ones
declare -A iso3Lang
iso3Lang[bg]=bul
iso3Lang[de]=deu
iso3Lang[el]=ell
iso3Lang[en]=eng
iso3Lang[es]=spa
iso3Lang[fi]=fin
iso3Lang[fr]=fra
iso3Lang[it]=ita
iso3Lang[ja]=jpn
iso3Lang[pl]=pol
iso3Lang[pt]=por
iso3Lang[ru]=rus
iso3Lang[tr]=tur
iso3Lang[nl]=nld
iso3Lang[sh]=shr
iso3Lang[sv]=swe
iso3Lang[lt]=lit
iso3Lang[id]=ind
iso3Lang[la]=lat
iso3Lang[mg]=mlg
iso3Lang[no]=nor
iso3Lang[bm]=bam
iso3Lang[ku]=kur

iso3Lang[bul]=bul
iso3Lang[deu]=deu
iso3Lang[ell]=ell
iso3Lang[eng]=eng
iso3Lang[spa]=spa
iso3Lang[fin]=fin
iso3Lang[fra]=fra
iso3Lang[ita]=ita
iso3Lang[jpn]=jpn
iso3Lang[pol]=pol
iso3Lang[por]=por
iso3Lang[rus]=rus
iso3Lang[tur]=tur
iso3Lang[nld]=nld
iso3Lang[shr]=shr
iso3Lang[swe]=swe
iso3Lang[lit]=lit
iso3Lang[ind]=ind
iso3Lang[lat]=lat
iso3Lang[mlg]=mlg
iso3Lang[nor]=nor
iso3Lang[bam]=bam
iso3Lang[kur]=kur


#
#  Decide on the graph in which a certain dump should be loaded
#
#  Usage:
#    write_graph LANGUAGE FEATURE DUMP

#  This decide the graph in which the dump should eb loaded and writes it 
# in a file names DUMP.graph alongside the dump.
#
#  Examples:
#    
#    write_graph fr exolex_ontolex database/load/fr_dbnary_exolex_ontolex.ttl
#
write_graph() {
    local lg=$1
    local feature=$2
    local fname=$3

    if [[ ${feature} == statistics ]]; then
        echo "http://kaiko.getalp.org/statistics" > "${fname}.graph"
    elif [[ ${feature} =~ exolex.* ]]; then
        lg3=${iso3Lang[${lg}]}
        echo "http://kaiko.getalp.org/dbnary/${lg3}_exolex" > "${fname}.graph"
    else 
        lg3=${iso3Lang[${lg}]}
        echo "http://kaiko.getalp.org/dbnary/${lg3}" > "${fname}.graph"
    fi
}

#
#  Download dataset from the web
#
#  Usage:
#    download LANGUAGE FEATURE...

#  This downloads a file from the web and expands it into the load folder
# it also create a .graph file to tell virtuoso in which graph the data 
# should be loaded.
#
#  Examples:
#    download "fr" ontolex lime exolex_ontolex
#
download() {
    local lg=$1
    shift
    local features=$@
    local LATEST=${DBNARY_HOME}/latest/
    for feature in $features
    do
        local fname=${lg}_dbnary_${feature}.ttl
        if [[ -f ${LATEST}/${fname}.bz2 ]]
        then
            bzcat < ${LATEST}/${fname}.bz2 > ${DATASETS}/${fname}
        else
            echo "Downloading ${fname}.bz2 from ${DBNARY_DUMPS_URL}/latest/"
            wget -q -O - ${DBNARY_DUMPS_URL}/latest/${fname}.bz2 | bzcat > ${DATASETS}/${fname}
        fi

        write_graph ${lg} ${feature} ${DATASETS}/${fname}
    done
}


FEATURES=${DBNARY_FEATURES}
if [[ -z ${FEATURES} ]]; then
    FEATURES=${DEFAULT_FEATURES}
fi
LANGUAGES=${DBNARY_LANGUAGES}
if [[ -z ${LANGUAGES} ]]; then
    LANGUAGES=${DEFAULT_LANGUAGES}
fi

echo "================================================"
echo "LOADING FEATURES : ${FEATURES}"
echo "FOR LANGUAGES : ${LANGUAGES}"
echo "================================================"

for l in $LANGUAGES; do
    echo "--------- LOADING $l"
    download ${l} ${FEATURES}
done

