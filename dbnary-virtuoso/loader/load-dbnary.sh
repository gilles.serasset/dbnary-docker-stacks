#!/bin/bash
#set -x
echo "============================================="
echo "PREPARING LOAD OF ALL PREPARED DATA"
echo "============================================="

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} <<END
ld_dir ('../datasets', '*.ttl', 'http://kaiko.getalp.org/dbnary');

-- do the following to see which files were registered to be added:
SELECT * FROM DB.DBA.LOAD_LIST;
-- if unsatisfied use:
-- delete from DB.DBA.LOAD_LIST;
echoln "========================================================" ;
echoln "=== Loading previously shown graphs                  ===" ;
echoln "========================================================" ;
END

echo "============================================="
echo "FORKING 4 PARALLEL LOADERS"
echo "============================================="

echo "rdf_loader_run();" > ./run_loader.rq
echo "forking loader"
"$ISQL" localhost:1111 dba ${DBA_PASSWORD} < ./run_loader.rq &
echo "forking loader"

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} < ./run_loader.rq &
echo "forking loader"

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} < ./run_loader.rq &
echo "forking loader"

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} < ./run_loader.rq &

echo "waiting loaders"
wait;
echo "All loaders done"

rm ./run_loader.rq

echo "============================================="
echo "COMMITING LOADS"
echo "============================================="

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} <<END
-- do nothing too heavy while data is loading
checkpoint;
commit WORK;
checkpoint;

echoln "========================================================" ;
echoln "=== Looking for errors while loading graphs                       ===" ;
echoln "========================================================" ;
-- Check the set of loaded files to see if errors appeared during load.
select * from DB.DBA.LOAD_LIST where ll_error IS NOT NULL;

echoln "=== Loading done                                     ===" ;
END

echo "============================================="
echo "CREATING ALL INDEXES"
echo "============================================="

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} <<END
echoln "========================================================" ;
echoln "=== Stats on loaded graphs                           ===" ;
echoln "========================================================" ;

sparql SELECT COUNT(*) WHERE { ?s ?p ?o } ;
sparql SELECT ?g COUNT(*) { GRAPH ?g {?s ?p ?o.} } GROUP BY ?g ORDER BY DESC 2;

echoln "========================================================" ;
echoln "=== Beginning full text indexing on loaded graphs    ===" ;
echoln "========================================================" ;

-- Build Full Text Indexes by running the following commands using the Virtuoso isql program
-- With this rule added, all text in all graphs will be indexed...
echoln "--- Setting up indexing"
RDF_OBJ_FT_RULE_ADD (null, null, 'All');
VT_INC_INDEX_DB_DBA_RDF_OBJ ();
echoln "--- Populating lookup table"
-- Run the following procedure using the Virtuoso isql program to populate label lookup tables periodically and activate the Label text box of the Entity Label Lookup tab:
urilbl_ac_init_db();
echoln "--- Ranking IRIs"
-- Run the following procedure using the Virtuoso isql program to calculate the IRI ranks. Note this should be run periodically as the data grows to re-rank the IRIs.
s_rank();
echoln "=== Indexing done                                    ===" ;
END

echo "============================================="
echo "BUILDING VARTRANS LINKS (NON HOMONYMOUS ENTRIES)"
echo "============================================="

"$ISQL" localhost:1111 dba ${DBA_PASSWORD} <<END
-- turn off transaction isolation to avoid reaching limits in transaction log
log_enable(2);
echoln "========================================================" ;
echoln "=== Linking translatableAs Lexical Entries           ===" ;
echoln "========================================================" ;
SPARQL INSERT
    { GRAPH <http://kaiko.getalp.org/dbnary/vartrans> {?sle vartrans:translatableAs ?tle} }
WHERE {
    { SELECT (sample(?sle) as ?sle), (sample(?le) as ?tle) WHERE {
      ?trans
        a dbnary:Translation ;
        dbnary:isTranslationOf ?sle ;
        dbnary:targetLanguage ?lg ;
        dbnary:writtenForm ?wf.
      ?sle a ontolex:LexicalEntry;
        lexinfo:partOfSpeech ?pos.
      ?le a ontolex:LexicalEntry;
        dcterms:language ?lg;
        ontolex:canonicalForm / ontolex:writtenRep ?wf;
        lexinfo:partOfSpeech ?pos.
      FILTER (REGEX(STR(?le), "^http://kaiko.getalp.org/dbnary/.../[^_]")) .
      } GROUP BY ?trans
        HAVING (COUNT(*) = 1)
    }
};
checkpoint;
commit WORK;
checkpoint;
echoln "=== Loading done                                     ===" ;
END
