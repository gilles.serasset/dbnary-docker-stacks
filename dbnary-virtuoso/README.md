# DBnary Virtuoso server docker image

Built from dbnary-docker-stacks: http://gitlab.com/gilles.serasset/dbnary-docker-stacks

This image runs a server that is configured for serving DBnary data. It uses the `develop/7` 
version of the virtuoso-opensource server and is compiled for `linux/arm64` and `linux/amd64`.

Openlink currently publishes a stable version of the server, with linux/amd64 architecture only. 

This image has been adapted from this official image, however check if this fits your need as 
this image is tailor made for DBnary data.

## Usage

### General information on this image

In order for the data to be persistent, we need to use a volume for storing the virtuoso database.

(NOTE: on mac os x (arm64 a least), there is a long lasting "bug" that make volumes mounted on the 
host filesystem very inefficient. Hence, on Mac OS X, use a docker volume for this (you may need 
to allow Docker Desktop to use a greater amount of disk in the preferences).)

#### Mount points

The potential mount points useful for this images are:
- `/opt/virtuoso-opensource/database`: the location of the virtuoso database. You should bind this to a docker volume or a filesystem folder to ensure persistence of the data.
- `/opt/virtuoso-opensource/settings`: if you did not provide a DBA/DAV password to the docker container, the image will generate a random password and write it in this folder. Should be bound to somewhere you can read if you do rely on this password generation feature.
- `/dbnary`: In order to speed up loading of DBnary data, bind this to the `ontolex` folder containing the DBnary data (organized the same way as on http://kaiko.getalp.org/static/ontolex). All data that is not available here will have to be downloaded from the kaiko web server.

#### Environment variables
Several environment variables may be used to control the behavior of the image.
- `VIRT_SECTION_KEY=VALUE`: Should be given when generating a new database to specify values to the different virtuoso.ini settings. 
  - where: 
    - VIRT is common prefix to group such variables together
    - SECTION is the name of the [section] in virtuoso.ini
    - KEY is the name of a key within the section
    - VALUE is the text to be written into the key
  - The variable names can be placed in either uppercase (most commonly used) or mixed case, without having to exactly match the case inside
  the virtuoso.ini file: `VIRT_Parameters_NumberOfBuffers` is same as `VIRT_PARAMETERS_NUMBEROFBUFFERS`
  - This allows you to adapt the memory usage of virtuoso server to your context (as virtuoso defaults are very low, but will lead to bad performance).
- `DBA_PASSWORD` and `DAV_PASSWORD`: use these to specify the dba and dav users passwords. If both are unset, a random password will be generated and stores in the settings folder. If only DDA_PASSWORD is set, the dav password will get the same password as dba.
- `DBNARY_LANGUAGES`: a space separated list of languages to be included. If unspecified all DBnary languages will be loaded in the database.
- `DBNARY_FEATURES`: a space separated list of features to be included in the database. Known features are: `ontolex`, `morphology`, `etymology`, `enhancement`, `lime`, `statistics`, `exolex_ontolex` and `exolex_lime`. If unspecified all features will be loaded in the database.

All these variables may be specified in the docker command line, through a file (with --file-env option) or with docker-compose.

For serving DBnary data, I advise you to set up a machine using at least 16G of RAM (i.e. `VIRT_PARAMETERS_NUMBEROFBUFFERS=1360000` and 
`VIRT_PARAMETERS_MAXDIRTYBUFFERS=1000000`).

#### PORTs open by the image

- `1111`: is the port used for connection through isql client
- `8890`: is the port for connecting through the web server

### Querying virtuoso version

`docker run -it --rm --disable-content-trust serasset/dbnary-virtuoso version`

### Loading data in a vanilla database

This command will load all data, using a mirror of kaiko at `$HOME/dev/wiktionary/tmp/ontolex` and persisting the database in a docker volume called `dbnary`. The `.env` file contains the buffer setting to give enough memory to the virtuoso server and the DBA/DAV passwords.

`docker run -it --rm --disable-content-trust --env-file .env -v dbnary:/opt/virtuoso-opensource/database -v $HOME/dev/wiktionary/tmp/ontolex:/dbnary serasset/dbnary-virtuoso load`

Example for a database with only French data:

`docker run -it --rm --disable-content-trust -e DBNARY_LANGUAGES="fr" --env-file .env -v dbnary_fr:/opt/virtuoso-opensource/database -v $HOME/dev/wiktionary/tmp/ontolex:/dbnary  serasset/dbnary-virtuoso load`

### Serving the loaded data

`docker run -it --rm --disable-content-trust -p8890:8890 -p1111:1111 -v dbnary:/opt/virtuoso-opensource/database serasset/dbnary-virtuoso start`

