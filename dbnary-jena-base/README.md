## DBnary Jena Base

A base image for dbnary dockers. This base image simply install jena with fuseki (with UI).

Intended to be used as a base image for other DBnary docker images.